import React from 'react'
import {SafeAreaView,Image, Text, TouchableOpacity, AsyncStorage, FlatList} from 'react-native'
import User from '../User'
import styles from '../constants/styles'
import firebase from 'firebase'

export default class HomeScreen extends React.Component{
    static navigationOptions = ({navigation}) => {
        return{
            title: 'Chats',
            headerRight: (
                <TouchableOpacity
                    onPress={()=> navigation.navigate('Profile')}
                    style={{padding:10, flexDirection:'row'}}>
                    <Image source={require('../images/cameraLogo.png')} style={{width:30, height:30, marginRight:18}}/>
                    <Image source={require('../images/messageLogo.png')} style={{width:25, height:25, marginRight:10}}/>
                </TouchableOpacity>
            )
        }
    }

    state = {
        users: [],
    }
    

    componentWillMount(){
        let dbRef = firebase.database().ref('users')
        dbRef.on('child_added', (val) => {
            let person = val.val();
            person.phone = val.key;
            if(person.phone === User.phone){
                User.name = person.name
            }else{
                this.setState((prevState) => {
                    return {
                        users: [...prevState.users, person]
                    }
                })
            }
        })
    }

    renderRow = ({item}) => {
        return (
            <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Chat',item)}
                style={{padding:10, borderBottomColor:'#cc', borderBottomWidth:1, flexDirection:'row'}}>
                <Image source={require('../images/user.png')} style={{width:40, height:40, marginRight:10}}/>
                <Text style={{fontSize:22, fontWeight: 'bold'}}>{item.name}</Text>
            </TouchableOpacity>
        )
    }
    render(){
        return(
            <SafeAreaView style = {styles.container}>
                <FlatList
                    style={{width:'100%'}}
                    data={this.state.users}
                    renderItem={this.renderRow}
                    keyExtractor={(item)=> item.phone}
                />
            </SafeAreaView>
        )
    }
}