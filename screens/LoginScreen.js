import React, {Component} from 'react';
import {
    Platform, StyleSheet, Text,
    AsyncStorage, TouchableOpacity,
    TextInput, View, Image
} from 'react-native';
import User from '../User'

import firebase from 'firebase'

export default class LoginScreen extends Component{
    static navigationOptions = {
        header: null
    }

    state = {
        phone:'',
        name:''
    }

    componentWillMount(){
        AsyncStorage.getItem('userPhone').then(val => {
            if(val){
                this.setState({phone:val})
            }
        })
    }

    handleChange = key => val =>{
        this.setState({[key]:val})
    }

    submitForm = async () => {

        if(this.state.name == '' | this.state.phone == ''){
            alert(this.state.phone + '\n'+this.state.name)
        }else{
            await AsyncStorage.setItem('userPhone', this.state.phone)
            User.phone = this.state.phone
            firebase.database().ref('users/'+User.phone).set({name:this.state.name})

            this.props.navigation.navigate('App')
            alert('phone successfully saved')
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={{paddingBottom: 130}}>
                <Image
                    style={{width: 70, height: 70}}
                    source={require('../images/logo_m.png')}
                />
                </View>
                <Text style={{fontWeight: 'bold', fontSize: 30}}>
                    Welcome to
                </Text>
                <Text style={{fontWeight: 'bold', fontSize: 30}}>
                    Messenger
                </Text>
                <View style={styles.nameInput}>
                    <TextInput
                        placeholder = "phone number"
                        style = {styles.input}
                        value = {this.state.phone}
                        onChangeText = {this.handleChange('phone')}
                    />
                    <TextInput
                        placeholder = "name"
                        style =  {styles.input}
                        value = {this.state.name}
                        onChangeText = {this.handleChange('name')}
                    />
                </View>
                <View style={styles.loginButton}>
                    <TouchableOpacity onPress={this.submitForm}>
                        <Text style={{fontWeight: 'bold'}}>Log In</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    input:{
        padding:10,
        borderWidth:1,
        borderColor:'#ccc',
        width: '90%',
        marginBottom:10,
        borderRadius:5
    },
    nameInput:{
        padding:50,
        paddingTop:80,
        borderWidth:0,
        borderColor:'#ccc',
        width: '90%',
        marginBottom:10,
        borderRadius:5
    },
    loginButton:{
        marginTop:10,
        paddingTop:15,
        paddingBottom:15,
        marginLeft:50,
        marginRight:50,
        backgroundColor:'#00BCD4',
        borderRadius:10,
        borderWidth: 1,
        borderColor: '#fff'
    },
    typeMessage:{
        padding:10,
        borderWidth:1,
        borderColor:'#ccc',
        width: '90%',
        marginBottom:10,
        borderRadius:50
    }
});